#!/bin/bash
# 
# profiles-common.sh
# 
# author: dooley@tacc.utexas.edu
#
# URL filter for openid connect services
#

filter_service_url() {
	if [[ -z $hosturl ]]; then
		if ((development)); then 
			hosturl="$devurl/oauth2/userinfo"
		else
			hosturl="$baseurl/oauth2/userinfo"
		fi
	fi
}

