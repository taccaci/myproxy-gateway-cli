#!/bin/bash
# 
# myproxy-common.sh
# 
# author: dooley@tacc.utexas.edu
#
# URL filter for apps services
#
if [[ -z "$X509_USER_PROXY" ]]; then
	X509_USER_PROXY="/tmp/x509up_u${UID}"
fi

if [[ -z "$X509_CERT_DIR" ]]; then
	X509_CERT_DIR="$HOME/.globus/certificates"
fi

if [ -f "$X509_CERT_DIR" ]; then
	mkdir -p $X509_CERT_DIR
fi
			
filter_service_url() {
	if [[ -z $hosturl ]]; then
		if ((development)); then 
			hosturl="$devurl/myproxy/"
		else
			hosturl="$baseurl/myproxy/1.0/"
		fi
	fi
}

