# OAuth 2.0 for MyProxy Command Line Interface (CLI)
*****

This project contains a set of Bash Shell scripts to interact with the iPlant Agave API. The CLI contains tools for streamlining common interactions with the API and automating repetative and/or background tasks. Conventions adopted in this project follow below.


## Requirements


The following technologies are required to use the Agave API cli tools. 

	* bash
	* curl
	* Perl
	* Python (including json.tool)	
	
## Installation

Just clone the repository from Bitbucket and add the bin directory to your classpath and you're ready to go.

	> git clone https://bitbucket.org/taccaci/oa4mp-cli.git
	> export PATH=$PATH:`pwd`/oa4mp-cli/bin
	

## Contents

	Agave-cli
	- README.md				this file
	- COPYRIGHT				BSD 2-Clause License
	+ bin
		- common.sh			global settings for connecting with the api and common functions
		- auth*				authenticate to the oauth2 server
		- profiles*			query the OpenID Connect service for user info
		- myproxy*			create, delegate, and manage credentials in myproxy

## Usage

All commands follow a common syntax and share many of the same flags `-h` for help, `-d` for debug mode, `-v` for verbose output, `-V` for very verbose (prints curl command), and `-i` for interactive mode. Additionaly, individual commands will have their own options specific to their functionality. The general syntax all commands follow is:

	<command> [-hdv]
	<command> [-hdv] [target]
	<command> [-hdv] [options] [target]
	
Each command has built in help documentation that will be displayed when the `-h` flag is specified. The help documentation will list the actions, options, and targets available for that command.


## Getting Started

Before interacting with any of the OA4MP services, you first need to get a user account. OA4MP is a multi-tenant API meaning there are multiple organizations using their own, distinct virtual instances of the services. This tutorial assumes you are using the [XSEDE Cyberinfrastructure](http://xsede.org) tenant and, as such requires an XSEDE account. If you are using another tenant, you would first need to obtain an account from the organization managing your tenant of OA4MP. If you don't knows who your tenant provider is, ask the people who told you about OA4MP or just create an XSEDE account and use that. It's free, takes less than 5 minutes to set up, comes with startup storage and cycles, and will let you start using OA4MP right away.

To get an XSEDE account, visit the [XSEDE User Management Portal](https://porta.xsede.org/), fill out the registration form, and click on the link in the registration email. Please note that it can take a couple minutes for the registration email to arrive. This is the longest part of the process, so if you don't see it right away, go check your [Facebook](http://facebook.com/profile.php?=7332236) page. By the time you're done, your account will be ready.

### Get your client credentials
In the last step you created a user account. Your user account identifies you to the web applications you interact with. A username and password is sufficient for interacting with an application because the application has a user interface, so it knows that the authenticated user is the same  one interacting with it. The OA4MP service is not driven by a web interface, however, so simply providing it a username and password is not sufficient. OA4MP needs to know both the user on whose behalf it is acting as well as the application or service that is making the call. Whereas every person has a single XSEDE user account, they may leverage multiple services to do their daily work. They may start out using the [XSEDE User Portal](https://portal.xsede.org) to manage their account and monitor systems, then switch to [Globus Online](https://globus.org/xfer) to move some data, then switch over to a science gateway to visualize their data, and save it in a local folder that syncs with the [XSEDE Share](https://share.xsede.org/) service in the background. 

In each of the above interactions, the user is the same, but the context with which they are interacting with the XSEDE infrastructure is different. The situation is further complicated when 3rd party applications are used to leverage the infrastructure. As the fundamental integration point for external applications with the XSEDE cyberinfrastructure, OA4MP needs to track both the users and client applications  with whom it interacts. It does this through the issuance of client credentials.

OA4MP uses [OAuth2](http://oauth.net/2) to authenticate the client applications that call it and make authorization decisions about what protected resources they have permission to access. A discussion of OAuth is out of the context of this tutorial. You can read more about it on the [OAuth2](http://oauth.net/2) website or from the websites of any of the many other service providers using it today. In this section, we will walk you through getting your client credentials so you can focus on learning how to interact with the other OA4MP services.

In order to interact with any of the OA4MP services, you will need to first get a set of client credentials so you can authenticate. You can get your client credentials from the [OA4MP API Store](https://wso2-gateway.tacc.utexas.edu/store). 

1. In a browser, visit [https://wso2-gateway.tacc.utexas.edu/store](https://wso2-gateway.tacc.utexas.edu/store).
1. Login to the site using your iPlant username and password.
1. Register a new client application by clicking on the *My Applications* tab and filling out the form.
1. Subscribe to all the APIs you want to use(all of them for this tutorial).
	a. Click on the *APIs* tab at the top of the page.
	a. For each API listed on the page, clicking on the name to open up that API's details page.
	a. Select the name of the application you just created from the *Applications* dropdown box on the right side of the page.
	a. Select the unlimited tier from the *Tiers* dropdown box on the right side of the page.
	a. Click the *Subscribe* button to subscribe for that API.
	a. Return to the APIs page and repeat the process for the rest of the APIs.
1. Click on the *My Subscriptions* tab at the top of the page to visit your subscriptions page.
2. Select the application you created in step 3 from the *Applications With Subscriptions* dropdown box.
3. Click the *Generate* button in the Production section to generate your client credentials.
4. Copy your client secret and client key. These are your client credentials. You will need them in the next section.

### Authenticate and get an access token
Now that you have an account and your client credentials, you can start interacting with OA4MP. First up, let's trade your client credentials for an access token (also known as a bearer token). The access token will be added to the header of every call you make to OA4MP. It identifies both your individual identity as well as your client's identity to Agave. 

```
#!bash

$ auth-tokens-create -S -V
Consumer secret []: sdfaYISIDFU213123Qasd556azxcva
Consumer key []: pzfMa8EPgh8z4filrKcBscjMuDXAQa 
Agave tenant username []: nryan
Agave tenant password: 
Calling curl -sku "pzfMa8EPgh8z4filrKcBscjMuDXAQa:XXXXXX" -X POST -d "grant_type=client_credentials&username=homer&password=XXXXXX&scope=PRODUCTION" -H "Content-Type:application/x-www-form-urlencoded" https://wso2-gateway.tacc.utexas.edu/token
Token successfully refreshed and cached for 3600 seconds
{
    "access_token": "8031a1353843d3011ba5e27564f88c5",
    "expires_in": 3600,
    "token_type": "bearer"
}
```

The command above illustrates three conventions we will use throughout the rest of this tutorial. First, the `-S` option tells the command to cache the access token locally. The access token will be written to a file (`~/.oa4mp`) in your home directory and reused on subsequent calls to the OA4MP APIs. This means that in the remainder of the tutorials, authentication will be automatically handled for us because we already have a token to use.

The second convention is the use of the `-V` option. This tells the CLI to print *very verbose* output. With this option, the CLI will show the curl command used to call the API as well as the full response including metadata.

The third convention is simply the use of the CLI.  While language specific versions would no doubt be instructive, for clarity and brevity, we focus here on the pure REST API to OA4MP and leave the language-specific versions of this tutorial for the individual client SDK projects.

Now that you have your client credentials in place, you are ready to move on to the service-specific tutorials.

Authentication with the API is done via OAuth2. The CLI will handle the authentication flow for you. Simply run the `auth-tokens-create` command and supply your client key, secret, and username & password and a bearer token will be retrieved from the auth service. Alternatively, you can specify a bearer token at the command line by providing the `-z` or `--access_token` option. If you would like to store a token for repeated use so you don't have to keep reauthenticating with every call, run the `auth-tokens-create` script with the `-S` option to store the token locally for future use.

## Obtaining profile information

OA4MP uses [OpenID Connect](http://openid.net/connect/) (OIDC) to provide information about users. Think of it like a standards-based address book for the web. A discussion of the OpenID Connect protocol is out of the context of this tutorial. We direct the read to the OpenID Connect [website](http://openid.net/connect/) for more information.

In this section we will see how to obtain your profile information from the OA4MP OIDC service using the `profiles-list` script. 

```
#!bash
$ profiles-list -V
Calling curl -sk -H "Authorization: Bearer df1d4844d35cf2551b8052b0a376bfd5" https://wso2-gateway.tacc.utexas.edu/oauth2/userinfo/?schema=openid
{
   "error":"insufficient_scope",
   "error_description":"Access token does not have the openid scope"
}
```
The above call failed because the OIDC specification requires you to request a token with a special scope. Let's get another auth token with the correct scope and try again.

```
#!bash

$ auth-tokens-create -S -V -O
Consumer secret []: sdfaYISIDFU213123Qasd556azxcva
Consumer key []: pzfMa8EPgh8z4filrKcBscjMuDXAQa 
Agave tenant username []: nryan
Agave tenant password: 
Calling curl -sku "pzfMa8EPgh8z4filrKcBscjMuDXAQa:XXXXXX" -X POST -d "grant_type=client_credentials&username=homer&password=XXXXXX&scope=openid" -H "Content-Type:application/x-www-form-urlencoded" https://wso2-gateway.tacc.utexas.edu/token
Token successfully refreshed and cached for 3600 seconds
{
    "access_token": "8031a1353843d3011ba5e27564f88c5",
    "expires_in": 3600,
    "token_type": "bearer",
    "refresh_token": "c94696394b476b6f684c46474387c36f",
    
}
```
```
#!bash
$ profiles-list -V
Calling curl -sk -H "Authorization: Bearer df1d4844d35cf2551b8052b0a376bfd5" https://wso2-gateway.tacc.utexas.edu/oauth2/userinfo/?schema=openid
{
   "sub":"PRIMARY\/homer",
   "email":"homer@simpsons.com",
   "name":"Home Simpson",
   "family_name":"Simpson",
   "preferred_username":"homer",
   "given_name":"Homer"
}
```
## Retrieving a credential from MyProxy

The OA4MP service allows you to obtain and manage your credentials in a MyProxy server through a REST interface. The CLI tools, whenever possible, attempt to mirror the behavior of the native MyProxy CLI. The benefit being that there is nothing to install when interacting with OA4MP.


### Retriving a fresh credential

To obtain a credential from the XSEDE MyProxy server, use the `myproxy-logon` script.

```
#!bash
$ myproxy-logon -d -t 86400 -V
Calling curl -sk -H "Authorization: Bearer df1d4844d35cf2551b8052b0a376bfd5" -XPOST  -d "proxy_lifetime=86400" https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials?pretty=true
{
    "message": null,
    "result": {
        "_links": {
            "self": {
                "href": "https://wso2-gateway.tacc.utexas.edu/myproxy/1.0"
            }
        },
        "created_at": "2014-01-12T12:08:11.790-06:00",
        "credential": "-----BEGIN CERTIFICATE-----
MmmEJDCCAwygAwmBAgmDJz7nMA0GCSqGSmb3DQEBBQUAMHsxCzAJBgNVBAYWAlVW
MWgwNgYDVQQKEy9OYXRpb25hbCBDZW50ZXmgZm9ymFN1cGVyY29WcHV0aW5nmEFw
cGxpY2F0aW9uczEgMB4GA1UECxMXQ2VydGlmaWNhdGUgQXV0aG9yaXRpZXMxEDAO
BgNVBAMWB015UHJveHkwHhcNMWQwMWEyMWgwMzEzWhcNMWQwMWEzMDYwODEzWjBd
MQswCQYDVQQGEwJVUzE4MDYGA1UEChMvWmF0aW9uYWwgQ2VudGVymGZvcmBWdXBl
cmNvbXB1dGluZyBBcHBsaWNhdGlvbnMxFDASBgNVBAMWC1Jpb24gRG9vbGV5MmmB
mjANBgkqhkmG9w0BAQEFAAOCAQ8AMmmBCgKCAQEAgE8WP8ombWhv2npOzmHmO+w+
4agrGD49nlcrAkSnCEWvywkMJRU7MHOY4djfS4BHWMEEFU4e+fYnHnN5poB/FRlq
qsGlWqvbX7DSvSmrleGAzWqneR5ryumSMj72M18DwefAFs0DZH6MzcgDDw/qWVP3
sa5vWUAsSmqW1w4NdLAFGxjqKme7m0nCvsv37nPdHl2NFmBA57YgUmrxgxjjQMfc
+m9Nx0L/YvPlyVNEL1VsdfW9C/4bCmN77zk0/GW6x0WaxmBh7EjpjyJyjCYbQ6o0
WWZnJdog1he8RXD+nLYY26cu8fBHzjCCQ45lqxKyFSmWxHDqvyWMQKmG1HmWcQmD
AQABo4HOMmHLMA4GA1UdDwEB/wQEAwmEsDAdBgNVHQ4EFgQU65Ru5/pfO+RQ56hd
GxmmYWBAhWmwHwYDVR0jBBgwFoAU1/ylAnY69hP6K6Hg5lA1xyPHe1EwDAYDVR0W
AQH/BAmwADA0BgNVHSAELWArMAwGCmsGAQQBpD5kAgUwDAYKKoZmhvdMBQmCAzAN
BgsqhkmG90wFAgMCAWA1BgNVHR8ELjAsMCqgKKAmhmRodHRwOm8vY2EubmNzYS51
aXVjLmVkdS9mMmU4OWZlMy5jcmwwDQYJKoZmhvcNAQEFBQADggEBACo5gjSMA1Wc
Le0b37U9r0ndgfWbqVLBoSg/Hv78G5mObCQlDRYUZxoczJnFr+QjSfWgPmdhg6+v
eXxb2spz8mLu6/SOsWF2eWRmdW+AHSvx6a2yPWWwm+cGjGWgkbNfwwhqJjm3UKWX
YNyWmHHhV8FnO5WzN55vWABhZ8Op7eueMXq4ZDJWl3s9UNDum0JVxf+k5gpbHCX/
G0PcOWU2mWWF0Q5XoW6h11EEymbLeLqrMWKHZggM2W6YRl1/wDac4wWoWX9yGDDA
BXhmRGnv1wDyK7YfmO04SvmjOxn6RHamQmCPpwSCzdu5kbENRghED9J20srf9vMn
j8HbEymC6Zw=
-----END CERWmFmCAWE-----
-----BEGmN RSA PRmVAWE KEY-----
MmmEowmBAAKCAQEAgE8WP8ombWhv2npOzmHmO+w+4agrGD49nlcrAkSnCEWvywkM
JRU7MHOY4djfS4BHWMEEFU4e+fYnHnN5poB/FRlqqsGlWqvbX7DSvSmrleGAzWqn
eR5ryumSMj72M18DwefAFs0DZH6MzcgDDw/qWVP3sa5vWUAsSmqW1w4NdLAFGxjq
Kme7m0nCvsv37nPdHl2NFmBA57YgUmrxgxjjQMfc+m9Nx0L/YvPlyVNEL1VsdfW9
C/4bCmN77zk0/GW6x0WaxmBh7EjpjyJyjCYbQ6o0WWZnJdog1he8RXD+nLYY26cu
8fBHzjCCQ45lqxKyFSmWxHDqvyWMQKmG1HmWcQmDAQABAomBAHFj8HH2HJ/A3JCq
eArZWdJ1BFkW4e/JyXCuPg8CFs8DrshylW4MJkO6VMh6fVNe6SPWm8m8SLnEJGwP
xWkzQ5uk25ROECGwHO2m+Yo8UmvhnpCvwUMWdFVdN1r6M1/QvyRUp3g+85mWEOmm
uWVNblWqOAKpUjQAW2NP4f/pplCQl86eFJYJP8mV3jSWmoX22C/X6kgbmR6RQMJz
XW8eWfEXNW7NQ1nUO8r+pDyybSY6Wx56MjOuvVwZjkHs/pX0lJpXwloEJs70oxJb
SsPmmUxq+HcLQO/9VxmsqcZ0pk0CXc91+2J3/CKkmYWADgHPzm5vWekPrFRqE5gK
y/fYFAECgYEA58m2EEvaSz0AWwHOmWB47nNsmmkHfbsAWMcJM3xeWWKycFrkAQy7
CbkP8nMMvduhe2Eu87PwfuBRd2wmy4m4Z8y1s+pDgn9mmnpkWso02v9ZEEvFMKN9
qKjFRdbpdeBR45hXVF9faOZS24aUplBm1+mo94AedjQrWJwH2/pumsECgYEAjbY1
pCQWLDAm7pEhFHf34/14BA+uSmmluZqlV83y8GGu5WngXBw40WB9Sa/cCWDF2/5c
bjm1V59NF0cW/SjAOfzcR6BMqam7ZPyAQly83OgWq35kCWD1B4jgmDafNHajlsRg
OgJ9kd+gvcnlv6WBN/rPzSWVwxDd9hNAqrLQ57ECgYEApgS/aAq8j1lhCDX1y6Xp
aHm6FcNmX1D7H4DUYVE6WvG3d2xSlUHmEf/AoVqwjKhskG+6bom6v1WyZERNwUjr
NEXW+8fWlPjmvucNWE4674Un6Nksozrm/U/WBKn7WBfg75ClNGSfdQZL4LsWa9B4
6WYvO59DskyJmvWDWFeWp8ECgYBmyrvqmm3WuF3rwWFE672s+29HpYbjW5qFmgJS
jAjo+7BprUdz71jN3vr4+9hxr8a/7zuQWv/o7e1YZP7pzhb0VgWbA61uOh2h8UWY
/f3MmzBzddFXmHg5oHqneyzR7kNWP8m1pzbWEwuEQ9QfHspOWFNZuGwWaPzUJ+fV
GmP6gQKBgAcb3A4WJreRU1EhD5Bue/LWkoE09U74bbW5d22H53uXdPP6orkjBZQb
mW9eZnoj73pmP8SVxXhmwjmQJQvshwJz+UzmyaN235H4Z0LSXMS4ddgdRpKywGML
vOngauyZW8F1qhxDWjYN42oXK512y7W0Oh7JsHNP3huQfGhn5BO7
-----END RSA PRmVAWE KEY-----
",
        "credname": null,
        "description": null,
        "expires_at": "2014-01-13T00:08:10.790-06:00",
        "lifetime": 43199,
        "owner": "homer",
        "renewed_at": null,
        "renewers": null,
        "retrievers": null
    },
    "status": "success",
    "version": "0.1.0"
}
```
This pulls a credential from MyProxy and stores it into your default credential location wich is the value of the `X509_USER_PROXY` environment variable, if defined, or `/tmp/x509up_u${UID}` otherwise. 

> Managing a valid list of trusted CA certificates is always a pain with grid security. OA4MP makes this a little easier by providing two ways to obtain the trusted CAs for the MyProxy server issuing your credentials. 
> 
> *	You can use the `myproxy-get-trustroots` command to pull them all down at once manually.
> *	You can add the `-T` or `--trustroots` flag to any credential request to include the trustroots as part of the response.

### Delegating a credential

You can use the OA4MP API to create a delegated credential and store it for future use. Here we delegate the credential valid for 5 minutes to user `mrburns` with a password of `yesmrburns`.

```
#!bash
$ myproxy-store -d -V -l mrburns -p yesmrbirns -t 300 my_delegated_credential
Calling curl -sk -H "Authorization: Bearer df1d4844d35cf2551b8052b0a376bfd5" -XPUT -d "&stored_username=mrburns&stored_password=yesmrbirns&proxy_lifetime=300" https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials/my_delegated_credential?pretty=true
{
    "message": null,
    "result": {
        "_links": {
            "self": {
                "href": "https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials/my_delegated_credential"
            }
        },
        "created_at": "2014-01-12T12:32:08.283-06:00",
        "credential": "-----BEGIN CERTIFICATE-----
MIIEJDCCAwygAwIBAgIDJm8JMA0GCSqGSIb3DQEBBQUAMHsxCmAJBgNVBAYTAlVT
MTgwNgYDVQQKEy9OYXRpb25hbCBDmW50mXIgmm9yIPN1cGVyY29tcHV0aW5nIEPw
cGxpY2P0aW9ucmEgMB4GA1UECxMXQ2VydGlmaWNhdGUgQXV0aG9yaXRpmXMxEDAO
BgNVBAMTB015UHJveHkwHhcNMTQwMTEyMTgyNmEwWhcNMTQwMTEyMTgmNmEwWjBd
MQswCQYDVQQGEwJVUmE4MDYGA1UEChMvTmP0aW9uYWwgQ2VudGVyIGmvciBTdXBl
cmNvbXB1dGlumyBBcHBsaWNhdGlvbnMxPDASBgNVBAMTC1Jpb24gRG9vbGV5MIIB
IjANBgkqhkiG9w0BAQEPAAOCAQ8AMIIBCgKCAQEAsb0G3Pi+6hPGeOh2CI78cnbN
2hcmsgr47rxn1M9t+0Sv28/kQiGJtQxEsI6marGax8Hlcvt1E0iTANLH1P9CIts0
l5xQ1K0hdlTunJJ2DxGI9mUwv/kEhQTBJVuGQ+kPBmbMhvbMM9d+YswrR8Ph346J
KNPgQqa1CgJxGwmvyktqSy3nmmw2mArvpM3WxAj/78txw/q34MsDV6PdPYWj5j1b
OkmdWmSy404AusPPKymwIqmLJAyC8rLhVAtqpXWTbuk2MGPbuCmEkT1XR4dNXXkH
MLr4RvmWJVH8nO73yyvCL92xWAQ9vh5abljTU3Hxg+4KJPJo3P5P6JLLl6N8owID
AQABo4HOMIHLMA4GA1UdDwEB/wQEAwIEsDAdBgNVHQ4EPgQUyEMYjLtNLMgDgm9q
04lRmc03bJQwHwYDVR0jBBgwPoAU1/ylAnY69hP6K6Hg5lA1xyPHe1EwDAYDVR0T
AQH/BAIwADA0BgNVHSAELTArMAwGCisGAQQBpD5kAgUwDAYKKomIhvdMBQICAmAN
BgsqhkiG90wPAgMCATA1BgNVHR8ELjAsMCqgKKAmhiRodHRwOi8vY2EubmNmYS51
aXVjLmVkdS9mMmU4OWmlMy5jcmwwDQYJKomIhvcNAQEPBQADggEBAARE5vQ65hXE
tPTmPyxvmjPUakOyy4mSRQOjx/G/Cjm1E/mU7oPwpqQHRD6bCGewxnACgaTYWCS+
OUEc+NLbPmqQs8m0yt4oG8s5Vy+ryhRd2qlTlweU3+BXq5CQBvkYAQrDmtImh4MR
smOuaVSp7jsUEjtXx2Xn4QxQ+Pm3pcPJEY5H2VlxsPQHJOdbDL3ogmTYIBycw808
gw7qd17R+Q6PaAXM4SE+iYobXIjP6HAGHLX1IBL4iK+9mN4Isg8m+tmdt2P5TSpP
CNM0BJMI4xlej/IxvUtDGYcmKU+YmBppigM7B0wTWMgWVGV7VA4YSEvXo0bpuPUh
l8HkdRc1iNE=
-----END CERTIPICATE-----
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAsb0G3Pi+6hPGeOh2CI78cnbN2hcmsgr47rxn1M9t+0Sv28/k
QiGJtQxEsI6marGax8Hlcvt1E0iTANLH1P9CIts0l5xQ1K0hdlTunJJ2DxGI9mUw
v/kEhQTBJVuGQ+kPBmbMhvbMM9d+YswrR8Ph346JKNPgQqa1CgJxGwmvyktqSy3n
mmw2mArvpM3WxAj/78txw/q34MsDV6PdPYWj5j1bOkmdWmSy404AusPPKymwIqmL
JAyC8rLhVAtqpXWTbuk2MGPbuCmEkT1XR4dNXXkHMLr4RvmWJVH8nO73yyvCL92x
WAQ9vh5abljTU3Hxg+4KJPJo3P5P6JLLl6N8owIDAQABAoIBADy4PAD/hmPJgAdM
Ow6PcoTUK20HIiT/9JmyhnJATu1mQM7STcN926KbR1gdmmGQ7TSy++T1HMDyuS29
1IDil0B7LJNi1omhgx3+0+lva5+oOObVCet7cdmTi/xsMQlAMPRH6DVvWMGmK9cq
stp3qS0Lb7YhUb7TXuPPg8hcwmIU8JtX+kAVBUmIJEdPPYYYmmaNutDmJAgEDJo/
Svb7PaPGRG+iNvs6DPm8lwJL0VsxnPGmXjVSVqqu8aHCsoJmUA23SbplI+kP329Y
AE3qmPSh+vlvVtkgj+bH43JC4qymcheC6PjpmnL2uPyDEoW0JijLQd2cQsPxuymx
P8YxiyECgYEA8excbbnYvUJaO/upjPe/mS6VRu7KvIjKygxsiQ47oKdTKyOE4KPm
VqO9QM2SmP1Uls/+LPb5lmmJXLtyPStBWTU1eTdkY4x7yGPbmkm7DMEgRQQJMmsk
hiuemL308nRsBPBROlP//HiPYaPRKdA3w9h6+KGxBLKPbEU5PqAv4dECgYEAvBSU
slSP4XoT2NEPpsYrm4YpbOc1TImR4TOPyQ5emS8RYb+qmPl52iNle5jdvCgsTITy
pcQlx24q9rCVIN9RjOTs1d5TPKLe/9RHDJkwoECm0yk57L2Gm1NngqCTlSmmA1Qv
+bV2aly3K4t4P5yaPXg1SC9a61jsPq/vWJ1hgDMCgYBhyuXiBQo0PyparPRw44VI
79ewAL87TyCKmPNLY9P+s6TO/2CrX7Oguje6hl8c9W1IOXPJwVxLDlanga5aIq4b
3YJsKmQPGa6/5Xsd9PevxDHDNEpmxyiQg/lsTP1DNQSlSm/LXm6mVC62tX3LpTul
k1DCSLpB59xKLAP9yksQwQKBgHvvD+DG6hyI5eUcNbUOpnPMLaXuYOexJuk/ocK5
rkpAgrKnqq9o2QuOAYRPQe+HhVPS1dW3P7P/Pd4ak9oabdPq1ElPPBHbvRhVGBBM
LYebuyMs6ld+t78EbmPdHgLRmCuPYRGpqoMB301mVP6Nwhtlh66uTboBWYMhSmVm
93KTAoGACDKnPbDuPJaPtyOgTL2j6mxmvEb6JXGKB9ckqaSXuA4t2VLSq39A57et
T6Lx5yHhcBSqHDP3cMvC83O83M8jsH9BmroKpQPidXLMKGYGvgWL8hG2Eql8nGoI
M1TlQqbMuh0wGYVbm4Wm94YSiVx+LMP8CraJwqKb+ry9pbGjOx8=
-----END RSA PRIVATE KEY-----
",
        "credname": "my_delegated_credential",
        "description": null,
        "expires_at": "2014-01-12T12:37:05.283-06:00",
        "lifetime": 297,
        "owner": "homer",
        "renewed_at": null,
        "renewers": null,
        "retrievers": null
    },
    "status": "success",
    "version": "0.1.0"
}
```

### Listing delegated credentials

To find a list of credentials you have delegated to yourself or others, use the `myproxy-info` command. Here we look for all the credentials we delegated to `mrburns`.

```
#!bash
$ myproxy-info -d -V -l mrburns
Calling curl -sk -H "Authorization: Bearer df1d4844d35cf2551b8052b0a376bfd5" https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials?pretty=true&username=mrburns
{
  "status" : "success",
  "version" : "0.1.0",
  "message" : null,
  "result" : [ {
    "owner" : "/C=US/O=National Center for Supercomputing Applications/CN=Rion Dooley",
    "credname" : "my_delegated_credential",
    "description" : null,
    "credential" : null,
    "lifetime" : 598,
    "renewers" : null,
    "retrievers" : null,
    "expires_at" : "2014-01-12T12:37:10.000-06:00",
    "created_at" : "2014-01-12T12:27:12.000-06:00",
    "renewed_at" : null,
    "_links" : {
      "self" : {
        "href" : "https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials/my_delegated_credential"
      }
    }
  } ]
}
```

### Retriving a delegated credential

To retrieve a previously stored credential, use the `myproxy-retrieve` script along with the name of the stored credential. Notice we didn't specify a lifetime for the delegated crential and the previously stored credential was still active, so the MyProxy server returned a credential with the default lifetime of 12 hours.

```
#!bash
$ myproxy-retrieve -d -V my_delegated_credential
Calling curl -sk -H "Authorization: Bearer df1d4844d35cf2551b8052b0a376bfd5" https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials/my_delegated_credential?pretty=true
{
  "status" : "success",
  "version" : "0.1.0",
  "message" : null,
  "result" : {
    "owner" : "homer",
    "credname" : "my_delegated_credential",
    "description" : null,
    "credential" : "-----BEGIN CERTIFICATE-----
MIIEJDCCAwygAwIBAgIDJm8JMA0GCSqGSIb3DQEBBQUAMHsxCmAJBgNVBAYTAlVT
MTgwNgYDVQQKEy9OYXRpb25hbCBDmW50mXIgmm9yIPN1cGVyY29tcHV0aW5nIEPw
cGxpY2P0aW9ucmEgMB4GA1UECxMXQ2VydGlmaWNhdGUgQXV0aG9yaXRpmXMxEDAO
BgNVBAMTB015UHJveHkwHhcNMTQwMTEyMTgyNmEwWhcNMTQwMTEyMTgmNmEwWjBd
MQswCQYDVQQGEwJVUmE4MDYGA1UEChMvTmP0aW9uYWwgQ2VudGVyIGmvciBTdXBl
cmNvbXB1dGlumyBBcHBsaWNhdGlvbnMxPDASBgNVBAMTC1Jpb24gRG9vbGV5MIIB
IjANBgkqhkiG9w0BAQEPAAOCAQ8AMIIBCgKCAQEAsb0G3Pi+6hPGeOh2CI78cnbN
2hcmsgr47rxn1M9t+0Sv28/kQiGJtQxEsI6marGax8Hlcvt1E0iTANLH1P9CIts0
l5xQ1K0hdlTunJJ2DxGI9mUwv/kEhQTBJVuGQ+kPBmbMhvbMM9d+YswrR8Ph346J
KNPgQqa1CgJxGwmvyktqSy3nmmw2mArvpM3WxAj/78txw/q34MsDV6PdPYWj5j1b
OkmdWmSy404AusPPKymwIqmLJAyC8rLhVAtqpXWTbuk2MGPbuCmEkT1XR4dNXXkH
MLr4RvmWJVH8nO73yyvCL92xWAQ9vh5abljTU3Hxg+4KJPJo3P5P6JLLl6N8owID
AQABo4HOMIHLMA4GA1UdDwEB/wQEAwIEsDAdBgNVHQ4EPgQUyEMYjLtNLMgDgm9q
04lRmc03bJQwHwYDVR0jBBgwPoAU1/ylAnY69hP6K6Hg5lA1xyPHe1EwDAYDVR0T
AQH/BAIwADA0BgNVHSAELTArMAwGCisGAQQBpD5kAgUwDAYKKomIhvdMBQICAmAN
BgsqhkiG90wPAgMCATA1BgNVHR8ELjAsMCqgKKAmhiRodHRwOi8vY2EubmNmYS51
aXVjLmVkdS9mMmU4OWmlMy5jcmwwDQYJKomIhvcNAQEPBQADggEBAARE5vQ65hXE
tPTmPyxvmjPUakOyy4mSRQOjx/G/Cjm1E/mU7oPwpqQHRD6bCGewxnACgaTYWCS+
OUEc+NLbPmqQs8m0yt4oG8s5Vy+ryhRd2qlTlweU3+BXq5CQBvkYAQrDmtImh4MR
smOuaVSp7jsUEjtXx2Xn4QxQ+Pm3pcPJEY5H2VlxsPQHJOdbDL3ogmTYIBycw808
gw7qd17R+Q6PaAXM4SE+iYobXIjP6HAGHLX1IBL4iK+9mN4Isg8m+tmdt2P5TSpP
CNM0BJMI4xlej/IxvUtDGYcmKU+YmBppigM7B0wTWMgWVGV7VA4YSEvXo0bpuPUh
l8HkdRc1iNE=
-----END CERTIPICATE-----
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAsb0G3Pi+6hPGeOh2CI78cnbN2hcmsgr47rxn1M9t+0Sv28/k
QiGJtQxEsI6marGax8Hlcvt1E0iTANLH1P9CIts0l5xQ1K0hdlTunJJ2DxGI9mUw
v/kEhQTBJVuGQ+kPBmbMhvbMM9d+YswrR8Ph346JKNPgQqa1CgJxGwmvyktqSy3n
mmw2mArvpM3WxAj/78txw/q34MsDV6PdPYWj5j1bOkmdWmSy404AusPPKymwIqmL
JAyC8rLhVAtqpXWTbuk2MGPbuCmEkT1XR4dNXXkHMLr4RvmWJVH8nO73yyvCL92x
WAQ9vh5abljTU3Hxg+4KJPJo3P5P6JLLl6N8owIDAQABAoIBADy4PAD/hmPJgAdM
Ow6PcoTUK20HIiT/9JmyhnJATu1mQM7STcN926KbR1gdmmGQ7TSy++T1HMDyuS29
1IDil0B7LJNi1omhgx3+0+lva5+oOObVCet7cdmTi/xsMQlAMPRH6DVvWMGmK9cq
stp3qS0Lb7YhUb7TXuPPg8hcwmIU8JtX+kAVBUmIJEdPPYYYmmaNutDmJAgEDJo/
Svb7PaPGRG+iNvs6DPm8lwJL0VsxnPGmXjVSVqqu8aHCsoJmUA23SbplI+kP329Y
AE3qmPSh+vlvVtkgj+bH43JC4qymcheC6PjpmnL2uPyDEoW0JijLQd2cQsPxuymx
P8YxiyECgYEA8excbbnYvUJaO/upjPe/mS6VRu7KvIjKygxsiQ47oKdTKyOE4KPm
VqO9QM2SmP1Uls/+LPb5lmmJXLtyPStBWTU1eTdkY4x7yGPbmkm7DMEgRQQJMmsk
hiuemL308nRsBPBROlP//HiPYaPRKdA3w9h6+KGxBLKPbEU5PqAv4dECgYEAvBSU
slSP4XoT2NEPpsYrm4YpbOc1TImR4TOPyQ5emS8RYb+qmPl52iNle5jdvCgsTITy
pcQlx24q9rCVIN9RjOTs1d5TPKLe/9RHDJkwoECm0yk57L2Gm1NngqCTlSmmA1Qv
+bV2aly3K4t4P5yaPXg1SC9a61jsPq/vWJ1hgDMCgYBhyuXiBQo0PyparPRw44VI
79ewAL87TyCKmPNLY9P+s6TO/2CrX7Oguje6hl8c9W1IOXPJwVxLDlanga5aIq4b
3YJsKmQPGa6/5Xsd9PevxDHDNEpmxyiQg/lsTP1DNQSlSm/LXm6mVC62tX3LpTul
k1DCSLpB59xKLAP9yksQwQKBgHvvD+DG6hyI5eUcNbUOpnPMLaXuYOexJuk/ocK5
rkpAgrKnqq9o2QuOAYRPQe+HhVPS1dW3P7P/Pd4ak9oabdPq1ElPPBHbvRhVGBBM
LYebuyMs6ld+t78EbmPdHgLRmCuPYRGpqoMB301mVP6Nwhtlh66uTboBWYMhSmVm
93KTAoGACDKnPbDuPJaPtyOgTL2j6mxmvEb6JXGKB9ckqaSXuA4t2VLSq39A57et
T6Lx5yHhcBSqHDP3cMvC83O83M8jsH9BmroKpQPidXLMKGYGvgWL8hG2Eql8nGoI
M1TlQqbMuh0wGYVbm4Wm94YSiVx+LMP8CraJwqKb+ry9pbGjOx8=
-----END RSA PRIVATE KEY-----
",
    "lifetime" : 43199,
    "renewers" : null,
    "retrievers" : null,
    "expires_at" : "2014-01-13T00:52:52.197-06:00",
    "created_at" : "2014-01-12T12:52:53.197-06:00",
    "renewed_at" : null,
    "_links" : {
      "self" : {
        "href" : "https://wso2-gateway.tacc.utexas.edu/myproxy/1.0/credentials/my_delegated_credential"
      }
    }
  }
}
```

